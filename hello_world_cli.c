#include <stdio.h>
#include <stdbool.h>

char* get_message1() {
	return "message1";
}

char* get_message2() {
	return "message2";
}

char* get_message3() {
	return "message3";
}

void next_message() {
	char* buffer;
	size_t len_buffer = 0;
	printf("Press enter if you want to see next message: ");
	getline(&buffer, &len_buffer, stdin); // Когда мы нажмём Enter, функция завершит своё выполнения
}

int main(int argc, char* argv[]) {
	printf("%s\n", get_message1());
	next_message();
	printf("%s\n", get_message2());
	next_message();
	printf("%s\n", get_message3());
	return 0;
}
